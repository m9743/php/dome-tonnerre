<?php
    spl_autoload_register(function($class_name) {
        $path_to_file = 'classes/' . $class_name . '.php';
    
        if (file_exists($path_to_file)) {
            require $path_to_file;
        }
    });

    $dome = Dome::getInstance();

    $dome->add(new Fighter("Brocoli", 500, 20, 50, 2));
    $dome->add(new Fighter("Carotte", 300, 40, 20, 4));
    $dome->add(new Fighter("Kiki", 2000, 10, 50, 5));
    $dome->add(new CretinusLapinus("Lapin Crétin", 5000, 100, 0, 9));
    $dome->addAll([new Fighter("Végétal", 450, 30, 30, 3), new Fighter("Ken", 750, 50, 10, 1)]);
    $dome->start();
?>