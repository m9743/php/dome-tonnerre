<?php
/**
 * Représente le lapin crétin qui attaque en rotant à la gueule
 */
class CretinusLapinus extends Fighter {

    
    public function hit(): bool {
        if($this->fumble < rand(0,10)) {
            echo $this->name . " <span style='color:green; font-weight:bold'>rote in the face !</span><br>";
            return true;
        }
        else {
            echo "<span style='color : red; font-weight:bold'>Echec critique</span> - " . $this->name . " se casse la gueule !<br>";
            return false;
        }
    }
}

?>