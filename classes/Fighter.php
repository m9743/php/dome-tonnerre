<?php
/**
 * Réprésente un combattant avec des caractéristiques
 */
class Fighter {
    /**
     * nom du combattant
     *
     * @var String
     */
    public String $name;
    /**
     * point de vie
     *
     * @var integer
     */
    public int $hp;
    /**
     * points d'attaque
     *
     * @var integer
     */
    public int $atk;
    /**
     * points de defense en % 
     *
     * @var integer valeur de 0 à 100
     */
    public int $def;
    /**
     * Probabilité d'échec critique
     *
     * @var integer valeur de 1 à 10
     */
    public int $fumble;



    public function __construct($name, $hp, $atk, $def, $fumble) {
        $this->name = $name;
        $this->hp = $hp;
        $this->atk = $atk;
        $this->def = $def;
        $this->fumble = $fumble;
    }


    
    /**
     * Decrease hp after the calculation of dmg when the fighter received a hit
     *
     * @param int $atk récupère l'attaque de l'attaquant
     * @return void
     */
    public function decreaseHp($atk): void {
        $atk -= $atk * ($this->def / 100);
        $this->hp -= $atk;

        echo $this->name . " perd <span style='font-weight:bold'>" . $atk . " PV</span> et a maintenant <span style='font-weight:bold'>" . $this->hp . "PV</span><br>";
    }

    /**
     * Vérifie si le combattant est mort
     *
     * @return boolean true si il est bien mort lol
     */
    public function isDead(): bool {
        if($this->hp <= 0) {
            echo "<span style='font-weight:bold'>" . $this->name . " est K.O</span><br><br>";
            return true;
        }
        return false;
    }

    /**
     * Octroie le succès de l'attaque aléatoirement en fonction de la probabilité d'échec critique de l'attaquant
     *
     * @return boolean true si l'attaque est un succès
     */
    public function hit(): bool {
        if($this->fumble < rand(0,10)) {
            echo $this->name . " <span style='color:green; font-weight:bold'>réussit à frapper !</span><br>";
            return true;
        }
        else {
            echo "<span style='color : red; font-weight:bold'>Echec critique</span> - " . $this->name . " se rate !<br>";
            return false;
        }
    }
}

?>